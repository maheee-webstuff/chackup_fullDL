package net.mahes.chackup.server.search;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.SortedNumericDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortedNumericSortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.QueryBuilder;

import net.mahes.chackup.model.Post;
import net.mahes.chackup.model.SearchResult;
import net.mahes.chackup.model.ThreadModel;

@Singleton
public class Search {

    private Analyzer analyzer;
    private Directory directory;
    private QueryBuilder queryBuilder;
    private IndexWriterConfig config;

    private IndexWriter iwriter;

    private DirectoryReader ireader = null;
    private IndexSearcher isearcher = null;

    @Inject
    public Search() {
    }

    public void init() throws CorruptIndexException, LockObtainFailedException, IOException {
        analyzer = new StandardAnalyzer();
        directory = new RAMDirectory();
        queryBuilder = new QueryBuilder(analyzer);
        config = new IndexWriterConfig(analyzer);

        iwriter = new IndexWriter(directory, config);
        iwriter.commit();
    }

    public synchronized void indexThread(ThreadModel thread) throws CorruptIndexException, IOException {
        if (thread == null || thread.getPosts() == null || thread.getPosts().size() == 0) {
            return;
        }
        iwriter.deleteDocuments(getExactNoQuery(thread.getNo()));

        Document document = new Document();

        document.add(new LongPoint("no_point", thread.getNo()));
        document.add(new LongPoint("lastModified_point", thread.getLast_modified()));
        document.add(new SortedNumericDocValuesField("lastModified_sort", thread.getLast_modified()));
        document.add(new StoredField("no", thread.getNo()));
        document.add(new StoredField("lastModified", thread.getLast_modified()));

        Post firstPost = thread.getPosts().get(0);
        document.add(new TextField("title", firstPost.getSemantic_url(), Store.YES));
        if (firstPost.getCom() != null) {
            document.add(new TextField("firstText", firstPost.getCom(), Store.YES));
        }
        document.add(new TextField("firstTim", "" + firstPost.getTim(), Store.YES));
        if (firstPost.getExt() != null) {
            document.add(new TextField("firstExt", firstPost.getExt(), Store.YES));
        }

        int videoCnt = 0;
        int imageCnt = 0;

        StringBuffer fullText = new StringBuffer();

        for (Post post : thread.getPosts()) {
            fullText.append(post.getCom());

            if (".webm".equals(post.getExt())) {
                ++videoCnt;
            } else if (post.getExt() != null) {
                ++imageCnt;
            }
        }

        document.add(new TextField("text", fullText.toString(), Store.NO));
        document.add(new LongPoint("videoCnt", videoCnt));
        document.add(new LongPoint("imageCnt", imageCnt));
        document.add(new LongPoint("postCnt", thread.getPosts().size()));

        iwriter.addDocument(document);
        iwriter.commit();

        resetIndexReader();
    }

    public synchronized List<SearchResult> getNewest(int noResults) throws IOException {
        createIndexReader();

        Query query = LongPoint.newRangeQuery("lastModified_point", Long.MIN_VALUE, Long.MAX_VALUE);
        Sort sort = new Sort(new SortedNumericSortField("lastModified_sort", SortField.Type.LONG, true));

        TopDocs topDocs = isearcher.search(query, noResults, sort);
        return prepareResults(topDocs.scoreDocs);
    }

    public synchronized List<SearchResult> search(String term, boolean text, boolean sortByDate, int noResults) throws IOException {
        createIndexReader();

        if (term == null || term.length() == 0) {
            return new LinkedList<>();
        }

        Query query;
        Query titleQuery = queryBuilder.createPhraseQuery("title", term);
        if (text) {
            Query textQuery = queryBuilder.createPhraseQuery("text", term);
            query = new BooleanQuery.Builder()
                    .add(titleQuery, Occur.SHOULD)
                    .add(textQuery, Occur.SHOULD)
                    .setMinimumNumberShouldMatch(1)
                    .build();
        } else {
            query = titleQuery;
        }

        Sort sort;
        if (sortByDate) {
            sort = new Sort(new SortField("lm", SortField.Type.LONG, true));
        } else {
            sort = Sort.RELEVANCE;
        }

        TopDocs topDocs = isearcher.search(query, noResults, sort);
        return prepareResults(topDocs.scoreDocs);
    }

    private Query getExactNoQuery(long no) {
        return LongPoint.newExactQuery("no_point", no);
    }

    private void resetIndexReader() {
        ireader = null;
        isearcher = null;
    }

    private void createIndexReader() throws IOException {
        if (ireader == null || isearcher == null) {
            ireader = DirectoryReader.open(directory);
            isearcher = new IndexSearcher(ireader);
        }
    }

    private List<SearchResult> prepareResults(ScoreDoc[] scoreDocs) throws IOException {
        LinkedList<SearchResult> results = new LinkedList<>();

        for (ScoreDoc doc : scoreDocs) {
            Document hitDoc = isearcher.doc(doc.doc);
            results.addLast(new SearchResult(
                    Long.parseLong(hitDoc.get("no")),
                    Long.parseLong(hitDoc.get("lastModified")),
                    hitDoc.get("title"),
                    hitDoc.get("firstText"),
                    Long.parseLong(hitDoc.get("firstTim")),
                    hitDoc.get("firstExt")));
        }

        return results;
    }

}
