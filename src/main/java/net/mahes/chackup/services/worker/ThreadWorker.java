package net.mahes.chackup.services.worker;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.Post;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.model.ThreadResult;
import net.mahes.chackup.server.search.Search;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Requester;
import net.mahes.chackup.services.Storage;
import net.mahes.chackup.util.PostNoComparator;

@Singleton
public class ThreadWorker extends AbstractWorker {

    private Config config;
    private Requester requester;
    private Backlog backlog;
    private Storage storage;
    private Search search;

    private PostNoComparator postNoComparator = new PostNoComparator();

    @Inject
    public ThreadWorker(Config config, Requester requester, Backlog backlog, Storage storage, Search search) {
        this.config = config;
        this.requester = requester;
        this.backlog = backlog;
        this.storage = storage;
        this.search = search;
    }

    public boolean handleNextThread() throws IOException {
        Logger.info("Refreshing next Thread ...");

        ThreadModel thread = backlog.takeThread();
        if (thread == null) {
            Logger.info("No Thread found! Backlog empty!");
            return false;
        }

        Logger.info("Refreshing Thread {} ...", thread.getNo());

        ThreadResult fullThread = requester.getThread(config.BOARD, thread.getNo());
        if (fullThread == null) {
            return true;
        }
        thread.setPosts(fullThread.getPosts());

        search.indexThread(thread);

        ThreadModel oldThread = storage.retrieveThread(thread.getNo());

        if (oldThread != null) {
            thread.setImagesLoaded(oldThread.isImagesLoaded());
        } else {
            thread.setImagesLoaded(false);
        }

        // merge posts and fill post backlog
        if (oldThread != null && oldThread.getPosts() != null && oldThread.getPosts().size() > 0) {
            thread.setPosts(mergePosts(thread, oldThread.getPosts(), thread.getPosts()));
        } else {
            backlogFirstPost(backlog, thread);
            if (thread.isImagesLoaded()) {
                backlogAllPostsExceptFirst(backlog, thread);
            }
        }

        storage.storeThread(thread);

        return true;
    }
    
    protected List<Post> mergePosts(ThreadModel thread, List<Post> oldPosts, List<Post> newPosts) {
        LinkedList<Post> result = new LinkedList<Post>();
        Post post;

        Collections.sort(oldPosts, postNoComparator);
        Collections.sort(newPosts, postNoComparator);

        int iOld = 0;
        int iNew = 0;

        while (iOld < oldPosts.size() || iNew < newPosts.size()) {
            boolean addNew;
            if (iOld >= oldPosts.size()) {
                addNew = true;
            } else if (iNew >= newPosts.size()) {
                addNew = false;
            } else {
                int comp = postNoComparator.compare(oldPosts.get(iOld), newPosts.get(iNew));
                addNew = comp > 0;
                if (comp == 0) {
                    ++iNew;
                }
            }

            if (addNew) {
                post = newPosts.get(iNew);
                result.add(post);
                ++iNew;

                if (thread.isImagesLoaded()) {
                    backlogPost(backlog, thread, post, false);
                }
            } else {
                post = oldPosts.get(iOld);
                result.add(post);
                ++iOld;
            }
        }

        return result;
    }

}
