package net.mahes.chackup;

import org.pmw.tinylog.Logger;

public abstract class StoppableThread extends Thread {
    protected boolean stop = false;

    public void stopThread() {
        this.stop = true;
    }

    public boolean isRunning() {
        return !stop;
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                executeStep();
            } catch (Exception e) {
                Logger.error(e);
                stopThread();
            }
        }
    }

    abstract protected void executeStep() throws Exception;
}