<#include "./parts/macros.ftl">

<#assign title = "Search Results">
<#include "./parts/header.ftl">

    <form method="GET" action="/search/">
        <input type="text" name="term">
        <input type="checkbox" name="full" value="true">
        <input type="submit">
    </form>


    <#list searchResults as searchResult>

        <@render_searchResult result=searchResult/>

    </#list>

<#include "./parts/footer.ftl">
