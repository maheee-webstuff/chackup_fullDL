package net.mahes.chackup.services;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.mahes.chackup.services.worker.PostWorker;
import net.mahes.chackup.services.worker.ThreadImageDownloadWorker;
import net.mahes.chackup.services.worker.ThreadWorker;
import net.mahes.chackup.services.worker.ThreadlistWorker;

@Singleton
public class Worker {

    private ThreadlistWorker threadlistWorker;
    private ThreadWorker threadWorker;
    private ThreadImageDownloadWorker threadImageDownloadWorker;
    private PostWorker postWorker;

    @Inject
    public Worker(ThreadlistWorker threadlistWorker, ThreadWorker threadWorker, ThreadImageDownloadWorker threadImageDownloadWorker, PostWorker postWorker) {
        this.threadlistWorker = threadlistWorker;
        this.threadWorker = threadWorker;
        this.threadImageDownloadWorker = threadImageDownloadWorker;
        this.postWorker = postWorker;
    }

    public long getLastRun() {
        return threadlistWorker.getLastRun();
    }

    public void setLastRun(long lastRun) {
        threadlistWorker.setLastRun(lastRun);
    }

    public void refreshThreadList() {
        threadlistWorker.refreshThreadList();
    }

    public boolean handleNextThread() throws IOException {
        return threadWorker.handleNextThread();
    }

    public void backlogPostsFromOldThreads() throws IOException {
        threadImageDownloadWorker.backlogPostsFromOldThreads();
    }
    
    public void handleNextPost() {
        postWorker.handleNextPost();
    }
}
