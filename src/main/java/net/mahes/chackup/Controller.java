package net.mahes.chackup;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import com.j256.simplejmx.common.JmxAttributeMethod;
import com.j256.simplejmx.common.JmxOperation;
import com.j256.simplejmx.common.JmxResource;

import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.server.Webserver;
import net.mahes.chackup.server.search.Search;
import net.mahes.chackup.services.Storage;
import net.mahes.chackup.services.Worker;

@Singleton
@JmxResource(domainName = "chackup")
public class Controller {

    private Provider<Worker> workerProvider;
    private Provider<Storage> storageProvider;
    private Provider<Webserver> webserverProvider;
    private Provider<Search> searchProvider;

    private Config config;
    private Worker worker;
    private Storage storage;
    private Webserver webserver;
    private Search search;

    private boolean webServerStarted = false;

    private UpdaterThread updaterThread;
    private DownloaderThread downloaderThread;

    @Inject
    public Controller(Config config, Provider<Worker> workerProvider, Provider<Storage> storageProvider, Provider<Webserver> webserverProvider, Provider<Search> searchProvider) {
        this.config = config;
        this.workerProvider = workerProvider;
        this.storageProvider = storageProvider;
        this.webserverProvider = webserverProvider;
        this.searchProvider = searchProvider;
    }

    public void init(int port, String host) throws IOException {
        this.worker = workerProvider.get();
        this.storage = storageProvider.get();
        this.webserver = webserverProvider.get();
        this.search = searchProvider.get();

        storage.init();
        webserver.init(port, host);
        search.init();
    }

    @JmxOperation()
    public void createIndex() throws Exception {
        Logger.info("Indexing Documents ...");
        for (String threadNo : storage.listThreads()) {
            ThreadModel thread = storage.retrieveThread(threadNo);
            search.indexThread(thread);
        }
        Logger.info("Indexing Documents done.");
    }

    @JmxOperation()
    public void startWebserver() {
        Logger.info("Starting Webserver ...");
        webserver.start();
        webServerStarted = true;
        Logger.info("Starting Webserver done.");
    }

    @JmxOperation()
    public void stopWebserver() {
        Logger.info("Stopping Webserver ...");
        
        if (isWebserverRunning()) {
            webserver.stop();
            webServerStarted = false;
            Logger.info("Stopping Webserver done.");
        } else {
            Logger.info("Webserver not running.");
        }
    }

    @JmxAttributeMethod()
    public boolean isWebserverRunning() {
        return webServerStarted;
    }

    @JmxOperation()
    public void startUpdater() throws Exception {
        Logger.info("Starting Updater ...");

        if (updaterThread != null) {
            if (updaterThread.isRunning()) {
                Logger.warn("Updater already running!");
                return;
            }
        }
        updaterThread = new UpdaterThread();
        updaterThread.start();

        Logger.info("Starting Updater done.");
    }

    @JmxOperation()
    public void stopUpdater() {
        Logger.info("Stopping Updater ...");

        if (isUpdaterRunning()) {
            updaterThread.stopThread();
            Logger.info("Stopping Updater done.");
        } else {
            Logger.info("Updater not running.");
        }
    }

    @JmxAttributeMethod()
    public boolean isUpdaterRunning() {
        return updaterThread != null && updaterThread.isRunning();
    }

    @JmxOperation()
    public void startImageDownloader() {
        Logger.info("Starting ImageDownloader ...");

        if (downloaderThread != null) {
            if (downloaderThread.isRunning()) {
                Logger.warn("ImageDownloader already running!");
                return;
            }
        }
        downloaderThread = new DownloaderThread();
        downloaderThread.start();

        Logger.info("Starting ImageDownloader done.");
    }

    @JmxOperation()
    public void stopImageDownloader() {
        Logger.info("Stopping ImageDownloader ...");

        if (isDownloaderRunning()) {
            downloaderThread.stopThread();
            Logger.info("Stopping ImageDownloader done.");
        } else {
            Logger.info("ImageDownloader not running.");
        }
    }

    @JmxAttributeMethod()
    public boolean isDownloaderRunning() {
        return downloaderThread != null && downloaderThread.isRunning();
    }

    /**
     * 
     */
    class UpdaterThread extends StoppableThread {

        @Override
        public void executeStep() throws Exception {
            worker.refreshThreadList();
            try {
                while (worker.handleNextThread()) {
                    Thread.sleep(config.UPDATER_REQUEST_PAUSE);
                }
            } catch (IOException e) {
                Logger.warn(e);
            } catch (Exception e) {
                Logger.warn(e);
            }
            try {
                worker.backlogPostsFromOldThreads();
            } catch (IOException e) {
                Logger.warn(e);
            } catch (Exception e) {
                Logger.warn(e);
            }
            Thread.sleep(config.UPDATER_RUN_PAUSE);
        }

    }

    /**
     * 
     */
    class DownloaderThread extends StoppableThread {

        @Override
        public void executeStep() throws Exception {
            worker.handleNextPost();
            Thread.sleep(config.DOWNLOADER_RUN_PAUSE);
        }

    }
}
