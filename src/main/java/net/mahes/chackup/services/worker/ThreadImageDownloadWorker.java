package net.mahes.chackup.services.worker;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Storage;

@Singleton
public class ThreadImageDownloadWorker extends AbstractWorker {

    private Backlog backlog;
    private Storage storage;

    @Inject
    public ThreadImageDownloadWorker(Backlog backlog, Storage storage) {
        this.backlog = backlog;
        this.storage = storage;
    }

    public void backlogPostsFromOldThreads() throws IOException {
        Long id;
        while ((id = backlog.takeThreadImageDownloadId()) != null) {
            backlogPostsFromOldThread(id);
        }
    }

    protected void backlogPostsFromOldThread(Long id) throws IOException {
        if (id == null) {
            return;
        }

        ThreadModel threadModel = storage.retrieveThread(id);
        if (threadModel == null || threadModel.isImagesLoaded()) {
            return;
        }

        backlogAllPostsExceptFirst(backlog, threadModel);

        threadModel.setImagesLoaded(true);

        storage.storeThread(threadModel);
    }

}
