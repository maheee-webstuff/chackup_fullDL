package net.mahes.chackup.util;

import com.google.gson.Gson;
import com.mashape.unirest.http.ObjectMapper;

public class GsonObjectMapper implements ObjectMapper {

    private Gson gson = new Gson();

    public <T> T readValue(String json, Class<T> valueType) {
        if (json.startsWith("[")) {
            json = "{result:" + json + "}";
        }
        return gson.fromJson(json, valueType);
    }

    public String writeValue(Object value) {
        return gson.toJson(value);
    }

}
