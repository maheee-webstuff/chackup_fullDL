package net.mahes.chackup.model;

public class SearchResult {

    private long no;
    private long lastModified;
    private String title;
    private String firstText;
    private long firstTim;
    private String firstExt;

    public SearchResult(long no, long lastModified, String title, String firstText, long firstTim, String firstExt) {
        this.no = no;
        this.lastModified = lastModified;
        this.title = title;
        this.firstText = firstText;
        this.firstTim = firstTim;
        this.firstExt = firstExt;
    }

    public long getNo() {
        return no;
    }

    public void setNo(long no) {
        this.no = no;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstText() {
        return firstText;
    }

    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    public long getFirstTim() {
        return firstTim;
    }

    public void setFirstTim(long firstTim) {
        this.firstTim = firstTim;
    }

    public String getFirstExt() {
        return firstExt;
    }

    public void setFirstExt(String firstExt) {
        this.firstExt = firstExt;
    }

    @Override
    public String toString() {
        return "SearchResult [no=" + no + ", lastModified=" + lastModified + ", title=" + title + ", firstText=" + firstText + ", firstTim=" + firstTim + ", firstExt=" + firstExt + "]";
    }

}
