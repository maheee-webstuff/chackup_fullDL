package net.mahes.chackup.server.template.utils;

public class StatusModel {
    private boolean webserverRunning;
    private boolean updaterRunning;
    private boolean downloaderRunning;
    private int threadBacklogSize;
    private int postBacklogSize;

    public StatusModel(boolean webserverRunning, boolean updaterRunning, boolean downloaderRunning, int threadBacklogSize, int postBacklogSize) {
        super();
        this.webserverRunning = webserverRunning;
        this.updaterRunning = updaterRunning;
        this.downloaderRunning = downloaderRunning;
        this.threadBacklogSize = threadBacklogSize;
        this.postBacklogSize = postBacklogSize;
    }

    public boolean isWebserverRunning() {
        return webserverRunning;
    }

    public boolean isUpdaterRunning() {
        return updaterRunning;
    }

    public boolean isDownloaderRunning() {
        return downloaderRunning;
    }

    public int getThreadBacklogSize() {
        return threadBacklogSize;
    }

    public int getPostBacklogSize() {
        return postBacklogSize;
    }

}
