package net.mahes.chackup;

import org.codejargon.feather.Feather;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;
import org.pmw.tinylog.policies.SizePolicy;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.pmw.tinylog.writers.RollingFileWriter;

import com.j256.simplejmx.server.JmxServer;
import com.mashape.unirest.http.Unirest;

import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Worker;
import net.mahes.chackup.util.GsonObjectMapper;

public class App {

    public static void main(String[] argv) throws Exception {
        Configurator.currentConfig()
                .removeAllWriters()
                .writer(new ConsoleWriter(), Level.INFO)
                .addWriter(new RollingFileWriter("log.txt", 1, new SizePolicy(1024 * 1024 /*byte*/)), Level.TRACE)
                .activate();

        CmdArguments args = new CmdArguments(argv);

        Logger.info("Starting Application ...");
        Logger.info("Initializing Object Mapper ...");
        Unirest.setObjectMapper(new GsonObjectMapper());

        Logger.info("Setting up DI Framework ...");
        Feather feather = Feather.with();

        Logger.info("Initializing Components ...");
        Controller controller = feather.instance(Controller.class);
        Backlog backlog = feather.instance(Backlog.class);
        Worker worker = feather.instance(Worker.class);
        Config config = feather.instance(Config.class);

        Unirest.setTimeouts(config.UPDATER_CONNECT_TIMEOUT, config.UPDATER_READ_TIMEOUT);
        worker.setLastRun(System.currentTimeMillis() / 1000);
        controller.init(args.webPort, args.webHost);

        Logger.info("Base system ready! Starting optional components ...");

        if (args.startJmxServer) {
            Logger.info("Starting JMX Server on port " + args.jmxPort + " ...");
            JmxServer jmxServer = new JmxServer(args.jmxPort);
            jmxServer.start();
            jmxServer.register(controller);
            jmxServer.register(backlog);
        }

        if (args.indexExistingData) {
            controller.createIndex();
        }
        if (args.startWebServer) {
            controller.startWebserver();
        }
        if (args.startUpdater) {
            controller.startUpdater();
        }
        if (args.startDownloader) {
            controller.startImageDownloader();
        }
    }

}
