package net.mahes.chackup.server;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import freemarker.template.TemplateException;
import net.mahes.chackup.Config;
import net.mahes.chackup.Controller;
import net.mahes.chackup.model.SearchResult;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.server.search.Search;
import net.mahes.chackup.server.template.PageBuilder;
import net.mahes.chackup.server.template.utils.StatusModel;
import net.mahes.chackup.services.Backlog;
import net.mahes.chackup.services.Storage;

@Singleton
public class RequestHandler {

    private Config config;
    private Storage storage;
    private Search search;
    private Controller controller;
    private Backlog backlog;
    private PageBuilder pageBuilder;

    @Inject
    public RequestHandler(Config config, Storage storage, Search search, Controller controller, Backlog backlog, PageBuilder pageBuilder) {
        this.config = config;
        this.storage = storage;
        this.search = search;
        this.controller = controller;
        this.backlog = backlog;
        this.pageBuilder = pageBuilder;
    }

    private StatusModel getStatus() {
        return new StatusModel(
                controller.isWebserverRunning(),
                controller.isUpdaterRunning(),
                controller.isDownloaderRunning(),
                backlog.getThreadBacklogSize(),
                backlog.getPostBacklogSize());
    }

    public void handleOverview(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
        List<SearchResult> searchResults = search.getNewest(config.MAX_RESULTS_OVERVIEW);

        pageBuilder.createOverviewPage(writer, searchResults, getStatus());
    }

    public void handleNewest(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
        List<SearchResult> searchResults = search.getNewest(config.MAX_RESULTS_NEWEST);

        pageBuilder.createNewestPage(writer, searchResults, getStatus());
    }

    public void handleSearch(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
        String searchTerm = null;
        boolean doFullSearch = false;

        Deque<String> searchTerms = queryParameter.get("term");
        Deque<String> fullSearch = queryParameter.get("full");

        if (searchTerms != null && searchTerms.size() > 0) {
            searchTerm = searchTerms.poll();
        }
        if (fullSearch != null) {
            doFullSearch = true;
        }

        List<SearchResult> searchResults = Collections.emptyList();
        if (searchTerm != null) {
            searchResults = search.search(searchTerm, doFullSearch, true, config.MAX_RESULTS_SEARCH);
        }

        pageBuilder.createResultPage(writer, searchResults, getStatus());
    }

    public void handleThread(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
        String no = path.replaceAll("/([0-9]+).*", "$1");

        try {
            backlog.put(Long.parseLong(no));
        } catch (NumberFormatException e) {
        }

        ThreadModel thread = storage.retrieveThread(no);

        pageBuilder.createThreadPage(writer, thread, getStatus());
    }
}
