package net.mahes.chackup.services;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.ThreadModel;

@Singleton
public class Storage {

    private Config config;
    private ObjectMapper objectMapper;

    private Path dataPath;
    private Path threadsPath;
    private Path imagesPath;

    @Inject
    public Storage(Config config, ObjectMapper objectMapper) {
        this.config = config;
        this.objectMapper = objectMapper;
    }

    public synchronized void init() throws IOException {
        dataPath = FileSystems.getDefault().getPath(config.FOLDER_DATA);
        threadsPath = FileSystems.getDefault().getPath(config.FOLDER_THREADS);
        imagesPath = FileSystems.getDefault().getPath(config.FOLDER_IMAGES);

        Files.createDirectories(dataPath);
        Files.createDirectories(threadsPath);
        Files.createDirectories(imagesPath);
    }

    private Path getStoragePath(String no) {
        return threadsPath.resolve("" + no + ".json");
    }

    public List<String> listThreads() throws IOException {
        return Files.list(threadsPath).map(new Function<Path, String>() {

            @Override
            public String apply(Path t) {
                return t.getFileName().toString().replaceFirst("^(.*).json$", "$1");
            }

        }).sorted().collect(Collectors.<String> toList());
    }

    public synchronized ThreadModel retrieveThread(long no) {
        return retrieveThread("" + no);
    }

    public synchronized ThreadModel retrieveThread(String no) {
        Path path = getStoragePath(no);

        try {
            String json = new String(Files.readAllBytes(path));
            return objectMapper.threadFromJson(json);
        } catch (NoSuchFileException e) {
            return null;
        } catch (Exception e) {
            Logger.info(no);
            Logger.warn(e);
            return null;
        }
    }

    public synchronized void storeThread(ThreadModel thread) throws IOException {
        Path path = getStoragePath("" + thread.getNo());
        String json = objectMapper.jsonFromThread(thread);

        Files.write(path, json.getBytes(),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

}
