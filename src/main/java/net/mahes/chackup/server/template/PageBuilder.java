package net.mahes.chackup.server.template;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import freemarker.core.TemplateNumberFormatFactory;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import net.mahes.chackup.Config;
import net.mahes.chackup.model.SearchResult;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.server.template.utils.DateTime1000NumberFormat;
import net.mahes.chackup.server.template.utils.DateTimeNumberFormat;
import net.mahes.chackup.server.template.utils.Model;
import net.mahes.chackup.server.template.utils.StatusModel;

@Singleton
public class PageBuilder {

    private Configuration configuration;

    @Inject
    public PageBuilder(Config config) throws IOException {
        configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setClassLoaderForTemplateLoading(ClassLoader.getSystemClassLoader(), config.FOLDER_TEMPLATES);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);

        Map<String, TemplateNumberFormatFactory> customNumberFormats = new TreeMap<String, TemplateNumberFormatFactory>();
        customNumberFormats.put("asDateTime", new DateTimeNumberFormat());
        customNumberFormats.put("asDateTime1000", new DateTime1000NumberFormat());
        configuration.setCustomNumberFormats(customNumberFormats);
    }

    private void processTemplate(Writer writer, String name, Model model) throws TemplateException, IOException {
        Template template = configuration.getTemplate(name);
        template.process(model, writer);
    }

    public void createOverviewPage(Writer writer, List<SearchResult> searchResults, StatusModel status) throws TemplateException, IOException {
        processTemplate(writer, "overview.ftl", new Model(null, searchResults, status));
    }

    public void createNewestPage(Writer writer, List<SearchResult> searchResults, StatusModel status) throws TemplateException, IOException {
        processTemplate(writer, "newest.ftl", new Model(null, searchResults, status));
    }

    public void createResultPage(Writer writer, List<SearchResult> searchResults, StatusModel status) throws TemplateException, IOException {
        processTemplate(writer, "results.ftl", new Model(null, searchResults, status));
    }

    public void createThreadPage(Writer writer, ThreadModel thread, StatusModel status) throws TemplateException, IOException {
        processTemplate(writer, "thread.ftl", new Model(thread, null, status));
    }

}
