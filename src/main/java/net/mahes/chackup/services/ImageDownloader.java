package net.mahes.chackup.services;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.pmw.tinylog.Logger;

import net.mahes.chackup.Config;
import net.mahes.chackup.model.Post;

@Singleton
public class ImageDownloader {

    private Config config;
    private UrlBuilder urlBuilder;

    private Path imagesPath;

    @Inject
    public ImageDownloader(Config config, UrlBuilder urlBuilder) {
        this.config = config;
        this.urlBuilder = urlBuilder;

        imagesPath = FileSystems.getDefault().getPath(config.FOLDER_IMAGES);
    }
    
    private ReadableByteChannel openChannel(URL url) throws IOException {
        URLConnection connection = url.openConnection();
        connection.setConnectTimeout(config.DOWNLOADER_CONNECT_TIMEOUT);
        connection.setReadTimeout(config.DOWNLOADER_READ_TIMEOUT);
        return Channels.newChannel(connection.getInputStream());
    }

    public void downloadImage(Post post) {
        ReadableByteChannel rbc = null;
        FileOutputStream fos = null;

        try {
            String urlString = urlBuilder.getImageUrl(config.BOARD, post.getTim(), post.getExt());
            URL website = new URL(urlString);

            Path target = imagesPath.resolve("" + post.getThreadNo());
            Files.createDirectories(target);
            target = target.resolve(post.getTim() + post.getExt());

            if (Files.exists(target)) {
                return;
            }

            rbc = openChannel(website);
            fos = new FileOutputStream(target.toFile());
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (Exception e) {
            Logger.warn("Failed to download image " + post.getTim() + post.getExt());
            Logger.trace(e);
        } finally {
            try {
                rbc.close();
            } catch (Exception e) {
            }
            try {
                fos.close();
            } catch (Exception e) {
            }
        }
    }
}
