package net.mahes.chackup.services.worker;

import net.mahes.chackup.model.Post;
import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.services.Backlog;

public class AbstractWorker {

    protected void backlogFirstPost(Backlog backlog, ThreadModel thread) {
        for (Post post : thread.getPosts()) {
            backlogPost(backlog, thread, post, true);
            return;
        }
    }

    protected void backlogAllPostsExceptFirst(Backlog backlog, ThreadModel thread) {
        boolean first = true;
        for (Post post : thread.getPosts()) {
            if (!first) {
                backlogPost(backlog, thread, post, first);
            }
            first = false;
        }
    }

    protected void backlogPost(Backlog backlog, ThreadModel thread, Post post, boolean priority) {
        if (post.getExt() != null) {
            backlog.put(thread, post, priority);
        }
    }

}
