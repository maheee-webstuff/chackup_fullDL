package net.mahes.chackup.model;

import java.util.List;

public class ThreadModel {

    private long no;
    private long last_modified;
    private List<Post> posts; // not directly filled via REST
    private boolean imagesLoaded; // not directly filled via REST

    public ThreadModel() {
    }

    public long getNo() {
        return no;
    }

    public void setNo(long no) {
        this.no = no;
    }

    public long getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(long last_modified) {
        this.last_modified = last_modified;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public boolean isImagesLoaded() {
        return imagesLoaded;
    }

    public void setImagesLoaded(boolean imagesLoaded) {
        this.imagesLoaded = imagesLoaded;
    }

}
