<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>${title}</title>
        <link rel="stylesheet" href="/res/style.css">
        <script src="/res/script.js"></script>
    </head>
    <body>
        <table border="0">
            <tr>
                <td width="50%"><h1><a href="/">//</a> <a href="">${title}</a></h1></td>

                <td>Thread Backlog: ${status.threadBacklogSize}</td>
                <td>Post Backlog: ${status.postBacklogSize}</td> 


                <#if status.webserverRunning>
                    <td>Webserver Running</td>
                <#else>
                    <td>Webserver Stopped</td>
                </#if>

                <#if status.updaterRunning>
                    <td><a href="?updater=stop">Updater Running</a></td>
                <#else>
                    <td><a href="?updater=start">Updater Stopped</a></td>
                </#if>

                <#if status.downloaderRunning>
                    <td><a href="?downloader=stop">Downloader Running</a></td>
                <#else>
                    <td><a href="?downloader=start">Downloader Stopped</a></td>
                </#if>

            </tr>
        </table>
