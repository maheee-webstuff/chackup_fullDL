package net.mahes.chackup.model;

import java.util.List;

public class Page {

    private int page;
    private List<ThreadModel> threads;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<ThreadModel> getThreads() {
        return threads;
    }

    public void setThreads(List<ThreadModel> threads) {
        this.threads = threads;
    }

}
