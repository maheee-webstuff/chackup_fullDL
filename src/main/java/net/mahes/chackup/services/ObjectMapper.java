package net.mahes.chackup.services;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.mahes.chackup.model.ThreadModel;
import net.mahes.chackup.util.GsonObjectMapper;

@Singleton
public class ObjectMapper {

    private GsonObjectMapper mapper = new GsonObjectMapper();

    @Inject
    public ObjectMapper() {
    }

    public ThreadModel threadFromJson(String json) {
        return mapper.readValue(json, ThreadModel.class);
    }

    public String jsonFromThread(ThreadModel thread) {
        return mapper.writeValue(thread);
    }

}
