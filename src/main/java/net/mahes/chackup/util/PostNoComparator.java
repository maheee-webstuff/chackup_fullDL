package net.mahes.chackup.util;

import java.util.Comparator;

import net.mahes.chackup.model.Post;

public class PostNoComparator implements Comparator<Post> {

    @Override
    public int compare(Post o1, Post o2) {
        return Long.compare(o1.getNo(), o2.getNo());
    }

}
