package net.mahes.chackup.server;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.channels.Channels;
import java.nio.file.FileSystems;
import java.util.Deque;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import freemarker.template.TemplateException;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.PathSeparatorHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.FileResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.server.handlers.resource.ResourceManager;
import io.undertow.util.Headers;
import net.mahes.chackup.Config;
import net.mahes.chackup.Controller;

@Singleton
public class Webserver {

    private abstract class RequestHandlerWrapper implements HttpHandler {

        private void evaluateParameter(Map<String, Deque<String>> queryParameter) throws Exception {
            Deque<String> content;
            /*
            content = queryParameter.get(config.CONTROL_PARAMETER_SERVER);
            if (content != null && !content.isEmpty()) {
                if (content.getFirst().equals(config.CONTROL_PARAMETER_START)) {
                    controller.startWebserver();
                } else if (content.getFirst().equals(config.CONTROL_PARAMETER_STOP)) {
                    controller.stopWebserver();
                }
            }
            */
            content = queryParameter.get(config.CONTROL_PARAMETER_UPDATER);
            if (content != null && !content.isEmpty()) {
                if (content.getFirst().equals(config.CONTROL_PARAMETER_START)) {
                    controller.startUpdater();
                } else if (content.getFirst().equals(config.CONTROL_PARAMETER_STOP)) {
                    controller.stopUpdater();
                }
            }

            content = queryParameter.get(config.CONTROL_PARAMETER_DOWNLOADER);
            if (content != null && !content.isEmpty()) {
                if (content.getFirst().equals(config.CONTROL_PARAMETER_START)) {
                    controller.startImageDownloader();
                } else if (content.getFirst().equals(config.CONTROL_PARAMETER_STOP)) {
                    controller.stopImageDownloader();
                }
            }
        }

        @Override
        public void handleRequest(HttpServerExchange exchange) throws Exception {
            Map<String, Deque<String>> queryParameter = exchange.getQueryParameters();
            evaluateParameter(queryParameter);

            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
            Writer writer = Channels.newWriter(exchange.getResponseChannel(), "UTF-8");
            handle(exchange.getRelativePath(), queryParameter, writer);
            writer.close();
        }

        public abstract void handle(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException;
    }

    private Config config;
    private Controller controller;
    private RequestHandler requestHandler;

    private Undertow server;

    @Inject
    public Webserver(Config config, Controller controller, RequestHandler requestHandler) {
        this.config = config;
        this.controller = controller;
        this.requestHandler = requestHandler;
    }

    public void init(int port, String host) {
        server = Undertow.builder()
                .addHttpListener(port, host)
                .setHandler(createHandler())
                .build();
    }

    public void start() {
        server.start();
    }

    public void stop() {
        server.stop();
    }

    private ResourceHandler createImageFileHandler() {
        File basePath = FileSystems.getDefault().getPath(config.FOLDER_IMAGES).toFile();
        ResourceManager imageResourceManager = new FileResourceManager(basePath, 0L);
        return new ResourceHandler(imageResourceManager);
    }

    private HttpHandler createOverviewHandler() {
        return new RequestHandlerWrapper() {
            @Override
            public void handle(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
                requestHandler.handleOverview(path, queryParameter, writer);
            }
        };
    }

    private HttpHandler createNewestHandler() {
        return new RequestHandlerWrapper() {
            @Override
            public void handle(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
                requestHandler.handleNewest(path, queryParameter, writer);
            }
        };
    }

    private HttpHandler createSearchHandler() {
        return new RequestHandlerWrapper() {
            @Override
            public void handle(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
                requestHandler.handleSearch(path, queryParameter, writer);
            }
        };
    }

    private HttpHandler createThreadHandler() {
        return new RequestHandlerWrapper() {
            @Override
            public void handle(String path, Map<String, Deque<String>> queryParameter, Writer writer) throws TemplateException, IOException {
                requestHandler.handleThread(path, queryParameter, writer);
            }
        };
    }

    private HttpHandler createResourceHandler() {
        ResourceManager resourceManager = new ClassPathResourceManager(ClassLoader.getSystemClassLoader(), config.FOLDER_WEBASSETS);
        return new ResourceHandler(resourceManager);
    }

    private HttpHandler createHandler() {
        PathHandler pathHandler = new PathHandler()
                .addPrefixPath("/images/", createImageFileHandler())
                .addPrefixPath("/thread/", createThreadHandler())
                .addPrefixPath("/res/", createResourceHandler())
                .addPrefixPath("/search/", createSearchHandler())
                .addPrefixPath("/newest", createNewestHandler())
                .addPrefixPath("/", createOverviewHandler());

        return new PathSeparatorHandler(pathHandler);
    }

}
